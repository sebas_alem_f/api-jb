# API RESTFUL CHALLENGE
## INSTALLING
This project has to execute over [Laravel/Homestead](https://laravel.com/docs/6.x/homestead)

### Database
1. Connect to Homestead database server 
2. Create a database `library`.
3. Configurate `.env` file for database connection.
4. Configurate `.env` file for `APP_URL` constant.

### Commands Init
1. Execute `composer install` 
2. Execute `php artisan migrate:refresh --seed`

## DOCUMENTACION
Open `APP_URL` in a browser.

## TESTING
Execute `phpunit` in root directory of project.

## LIMIT RATE 
Configure `app/Http/Kernel.php` variable `middlewaregroups` variable `throttle:rate_limit,lapse_time`
    
