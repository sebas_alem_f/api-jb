<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Book routers
 */
Route::resource('books', 'Book\BookController',
    ['except' => ['create', 'edit']]);
Route::resource('books.authors', 'Book\BookAuthorController',
    ['only' => ['index', 'update', 'destroy']]);

/**
 * Author routers
 */
Route::resource('authors', 'Author\AuthorController',
    ['except' => ['create', 'edit']]);
Route::resource('authors.books', 'Author\AuthorBookController',
    ['only' => ['index']]);
