<?php

use App\User;
use App\Author;
use App\Book;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        User::truncate();
        Author::truncate();
        Book::truncate();
        DB::table('author_book')->truncate();
        factory(Author::class, 5)->create();
        factory(Book::class, 21)->create()->each(
            function ($book) {
                $authors = Author::all()->random(1, 3)->pluck('id');
                $book->authors()->attach($authors);
            }
        );
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
