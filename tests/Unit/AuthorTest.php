<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthorTest extends TestCase
{
    use WithFaker;

    /**
     * All authors
     */
    public function testAll()
    {
        $response = $this->get('/authors');
        $response->assertStatus(200);
    }

    /**
     * Store Author
     */
    public function testStore()
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->post('/authors', [
            'name'     => $this->faker->firstName,
            'lastname' => $this->faker->lastName,
        ]);
        $response->assertStatus(201)->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'lastname',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
        ]);

        return json_decode($response->getContent());
    }

    /**
     * Store Author
     *
     */
    public function testStoreWithoutLastname()
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->post('/authors', [
            'name' => $this->faker->firstName,
        ]);
        $response->assertStatus(422)->assertJsonStructure([
            'error',
            'code',
        ]);
    }

    /**
     * Store Author
     */
    public function testStoreWithoutName()
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->post('/authors', [
            'lastname' => $this->faker->lastName,
        ]);
        $response->assertStatus(422)->assertJsonStructure([
            'error',
            'code',
        ]);
    }

    /**
     * Store Author
     */
    public function testStoreEmpty()
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->post('/authors', [
        ]);
        $response->assertStatus(422)->assertJsonStructure([
            'error',
            'code',
        ]);
    }

    /**
     * Update Author
     *
     * @depends testStore
     */
    public function testUpdate($stack)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->put('/authors/'.$stack->data->id, [
            'name'     => $this->faker->firstName,
            'lastname' => $this->faker->lastName,
        ], ['CONTENT_TYPE' => 'application/x-www-form-urlencoded']);
        $response->assertStatus(200)->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'lastname',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
        ]);

        return json_decode($response->getContent());
    }

    /**
     * Update Author
     *
     * @depends testUpdate
     */
    public function testUpdateWithoutName($stack)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->put('/authors/'.$stack->data->id, [
            'name' => $this->faker->firstName,
        ], ['CONTENT_TYPE' => 'application/x-www-form-urlencoded']);
        $response->assertStatus(200)->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'lastname',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
        ]);

        return json_decode($response->getContent());
    }

    /**
     * Update Author
     *
     * @depends testUpdateWithoutName
     */
    public function testUpdateWithoutLastname($stack)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->put('/authors/'.$stack->data->id, [
            'lastname' => $this->faker->lastName,
        ], ['CONTENT_TYPE' => 'application/x-www-form-urlencoded']);
        $response->assertStatus(200)->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'lastname',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
        ]);

        return json_decode($response->getContent());
    }

    /**
     * Update Author
     *
     * @depends testUpdateWithoutLastname
     */
    public function testUpdateEmpty($stack)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->put('/authors/'.$stack->data->id, [
        ], ['CONTENT_TYPE' => 'application/x-www-form-urlencoded']);
        $response->assertStatus(422)->assertJsonStructure([
            'error',
            'code',
        ]);
    }

    /**
     * Store Author
     */
    public function testUpdatenotFound()
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->put('/authors/999', [
            'name'     => $this->faker->firstName,
            'lastname' => $this->faker->lastName,
        ], ['CONTENT_TYPE' => 'application/x-www-form-urlencoded']);
        $response->assertStatus(404)->assertJsonStructure([
            'error',
            'code',
        ]);
    }

    /**
     * Destroy Author
     *
     * @depends testStore
     */
    public function testDestroy($stack)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->delete('/authors/'.$stack->data->id);
        $response->assertStatus(200)->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'lastname',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
        ]);
    }

    /**
     * Destroy Author
     *
     * @depends testStore
     */
    public function testDestroyNotFound($stack)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->delete('/authors/'.$stack->data->id);
        $response->assertStatus(404)->assertJsonStructure([
            'error',
            'code',
        ]);
    }
}
