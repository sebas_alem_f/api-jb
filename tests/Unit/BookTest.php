<?php

namespace Tests\Unit;

use App\Author;
use App\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BookTest extends TestCase
{
    use WithFaker;

    /**
     * All books
     */
    public function testAllAuthors()
    {
        $response = $this->get('books');
        $response->assertStatus(200);
    }

    /**
     * Store Book
     */
    public function testStore()
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->post('/books', [
            'title'    => $this->faker->word,
            'abstract' => $this->faker->paragraph(1),
            'isbn'     => $this->faker->isbn10,
            'price'    => $this->faker->randomNumber(2),
        ]);
        $response->assertStatus(201)->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'abstract',
                'isbn',
                'price',
                'status',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
        ]);

        return json_decode($response->getContent());
    }

    /**
     * @depends testStore
     */
    public function testFindByISBN($stack)
    {
        $response = $this->get('books?isbn='.$stack->data->isbn);
        $response->assertStatus(200)->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'title',
                    'abstract',
                    'isbn',
                    'price',
                    'status',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ],
            ],
            'meta' => [
                "pagination" => [
                    "total",
                    "count",
                    "per_page",
                    "current_page",
                    "total_pages",
                    "links",
                ],
            ],
        ]);

    }

    /**
     * @depends testStore
     */
    public function testFindByTitle($stack)
    {
        $response = $this->get('books?title='.$stack->data->title);
        $response->assertStatus(200)->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'title',
                    'abstract',
                    'isbn',
                    'price',
                    'status',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ],
            ],
            'meta' => [
                "pagination" => [
                    "total",
                    "count",
                    "per_page",
                    "current_page",
                    "total_pages",
                    "links",
                ],
            ],
        ]);
    }

    /**
     * @return mixed
     */
    public function testStoreWithOutTitle()
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->post('/books', [
            'abstract' => $this->faker->paragraph(1),
            'isbn'     => $this->faker->isbn10,
            'price'    => $this->faker->randomNumber(2),
        ]);
        $response->assertStatus(422)->assertJsonStructure([
            'error',
            'code',
        ]);

        return json_decode($response->getContent());
    }

    public function testStoreWithOutAbstract()
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->post('/books', [
            'title' => $this->faker->word,
            'isbn'  => $this->faker->isbn10,
            'price' => $this->faker->randomNumber(2),
        ]);
        $response->assertStatus(422)->assertJsonStructure([
            'error',
            'code',
        ]);

        return json_decode($response->getContent());
    }

    public function testStoreWithOutPrice()
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->post('/books', [
            'title'    => $this->faker->word,
            'abstract' => $this->faker->paragraph(1),
            'isbn'     => $this->faker->isbn10,
        ]);
        $response->assertStatus(201)->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'abstract',
                'isbn',
                'price',
                'status',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
        ]);

        return json_decode($response->getContent());
    }

    public function testStoreWithOutIsbn()
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->post('/books', [
            'title'    => $this->faker->word,
            'abstract' => $this->faker->paragraph(1),
            'price'    => $this->faker->randomNumber(2),
        ]);
        $response->assertStatus(422)->assertJsonStructure([
            'error',
            'code',
        ]);

        return json_decode($response->getContent());
    }

    public function testStoreEmpty()
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->post('/books', []);
        $response->assertStatus(422)->assertJsonStructure([
            'error',
            'code',
        ]);

        return json_decode($response->getContent());
    }

    /**
     * Update BookAuthor
     *
     * @depends testStore
     */
    public function testUpdateAuthorBook($stack)
    {
        $author = Author::all()->random(1)->first();
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->patch('/books/'.$stack->data->id.'/authors/'.$author->id);
        $response->assertStatus(200);

        return $author;
    }

    /**
     * Destroy BookAuthor
     *
     * @depends testStore
     * @depends testUpdateAuthorBook
     */
    public function testDestroyAuthorBook($stack, $author)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->delete('/books/'.$stack->data->id.'/authors/'.$author->id, []);
        $response->assertStatus(200);

        return json_decode($response->getContent());
    }


    /**
     * Store Book
     *
     * @depends testStore
     */
    public function testUpdate($stack)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->put('/books/'.$stack->data->id, [
            'title'    => $this->faker->word,
            'abstract' => $this->faker->paragraph(1),
            'isbn'     => $this->faker->isbn10,
            'price'    => $this->faker->randomNumber(2),
            'status'   => $this->faker->randomElement([
                Book::STATUS_AVAILABLE, Book::STATUS_UNAVAILABLE,
            ]),
        ], ['CONTENT_TYPE' => 'application/x-www-form-urlencoded']);
        $response->assertStatus(200)->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'abstract',
                'isbn',
                'price',
                'status',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
        ]);

        return json_decode($response->getContent());
    }

    /**
     * @param $stack
     *
     * @depends testUpdate
     * @return mixed
     */
    public function testUpdateWithOutTitle($stack)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->put('/books/'.$stack->data->id, [
            'abstract' => $this->faker->paragraph(1),
            'isbn'     => $this->faker->isbn10,
            'price'    => $this->faker->randomNumber(2),
            'status'   => $this->faker->randomElement([
                Book::STATUS_AVAILABLE, Book::STATUS_UNAVAILABLE,
            ]),
        ], ['CONTENT_TYPE' => 'application/x-www-form-urlencoded']);
        $response->assertStatus(200)->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'abstract',
                'isbn',
                'price',
                'status',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
        ]);

        return json_decode($response->getContent());
    }

    /**
     * @param $stack
     *
     * @depends testUpdate
     * @return mixed
     */
    public function testUpdateEmpty($stack)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->put('/books/'.$stack->data->id, [],
            ['CONTENT_TYPE' => 'application/x-www-form-urlencoded']);
        $response->assertStatus(422)->assertJsonStructure([
            'error',
            'code',
        ]);

        return json_decode($response->getContent());
    }

    /**
     * Destroy Book
     *
     * @depends testStore
     */
    public function testDestroy($stack)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->delete('/books/'.$stack->data->id);
        $response->assertStatus(200)->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'abstract',
                'isbn',
                'price',
                'status',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
        ]);
    }

    /**
     * Destroy Book
     *
     * @depends testStore
     */
    public function testDestroyNotFound($stack)
    {
        $response = $this->withHeaders([
            'X-Header' => 'UnitTest',
        ])->delete('/authors/'.$stack->data->id);
        $response->assertStatus(404)->assertJsonStructure([
            'error',
            'code',
        ]);
    }
}
