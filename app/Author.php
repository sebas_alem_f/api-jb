<?php

namespace App;

use App\Transformers\AuthorTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use SoftDeletes;
    protected $fillable
        = [
            'name',
            'lastname',
        ];
    protected $dates = ['deleted_at'];
    protected $hidden = ['pivot'];

    public $transformer = AuthorTransformer::class;

    /**
     * Set name lowercase
     *
     * @param $name
     */
    public function setNameAttribute($name)
    {
        $this->attributes['name'] = strtolower($name);
    }

    /**
     * Set lastname lowercase
     *
     * @param $lastname
     */
    public function setLastnameAttribute($lastname)
    {
        $this->attributes['lastname'] = strtolower($lastname);
    }

    /**
     * Get name capitalize
     *
     * @param $name
     *
     * @return string
     */
    public function getNameAttribute($name)
    {
        return ucwords($name);
    }

    /**
     * Get lastname capitalize
     *
     * @param $lastname
     *
     * @return string
     */
    public function getLastnameAttribute($lastname)
    {
        return ucwords($lastname);
    }

    /**
     * Relation many to many with Book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
