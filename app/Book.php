<?php

namespace App;

use App\Author;
use App\Transformers\BookTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;
    const STATUS_AVAILABLE = 'AVAILABLE';
    const STATUS_UNAVAILABLE = 'UNAVAILABLE';
    protected $dates = ['deleted_at'];
    protected $hidden = ['pivot'];
    protected $fillable
        = [
            'title',
            'description',
            'isbn',
            'price',
            'status',
        ];
    public $transformer = BookTransformer::class;


    /**
     * Set title lowercase
     *
     * @param $title
     */
    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = strtolower($title);
    }

    /**
     * Get title as uppercase
     *
     * @param $title
     *
     * @return string
     */
    public function getTitleAttribute($title)
    {
        return strtoupper($title);
    }

    /**
     * Relation many to many with Author
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }

    /**
     * Check a book its availability
     *
     * @return bool
     */
    public function isAvailable()
    {
        return $this->status == Book::STATUS_AVAILABLE;
    }
}
