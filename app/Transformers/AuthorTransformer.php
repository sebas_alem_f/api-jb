<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Author;

class AuthorTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Author $author)
    {
        return [
            'id'         => (int) $author->id,
            'name'       => (string) $author->name,
            'lastname'   => (string) $author->lastname,
            'created_at' => (string) $author->created_at,
            'updated_at' => (string) $author->updated_at,
            'deleted_at' => isset($author->deleted_at)
                ? (string) $author->deleted_at : null,
        ];
    }

    /**
     * @param $index
     *
     * @return mixed|null
     */
    public static function originalAttribute($index)
    {
        $attributes = [
            'id'         => 'id',
            'name'       => 'name',
            'lastname'   => 'lastname',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'deleted_at' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * @param $index
     *
     * @return mixed|null
     */
    public static function transformedAttribute($index)
    {
        $attributes = [
            'id'         => 'id',
            'name'       => 'name',
            'lastname'   => 'lastname',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'deleted_at' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
