<?php

namespace App\Transformers;

use App\Book;
use League\Fractal\TransformerAbstract;

class BookTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Book $book)
    {
        return [
            'id'         => (int) $book->id,
            'title'      => (string) $book->title,
            'abstract'   => (string) $book->description,
            'isbn'       => (string) $book->isbn,
            'price'      => floatval($book->price),
            'status'     => (string) $book->status,
            'created_at' => (string) $book->created_at,
            'updated_at' => (string) $book->updated_at,
            'deleted_at' => isset($book->deleted_at)
                ? (string) $book->deleted_at : null,
        ];
    }

    /**
     * @param $index
     *
     * @return mixed|null
     */
    public static function originalAttribute($index)
    {
        $attributes = [
            'id'         => 'id',
            'title'      => 'title',
            'abstract'   => 'description',
            'isbn'       => 'isbn',
            'price'      => 'price',
            'status'     => 'status',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'deleted_at' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * @param $index
     *
     * @return mixed|null
     */
    public static function transformedAttribute($index)
    {
        $attributes = [
            'id'          => 'id',
            'title'       => 'title',
            'description' => 'abstract',
            'isbn'        => 'isbn',
            'price'       => 'price',
            'status'      => 'status',
            'created_at'  => 'created_at',
            'updated_at'  => 'updated_at',
            'deleted_at'  => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
