<?php

namespace App\Http\Controllers\Book;

use App\Book;
use App\Http\Controllers\ApiController;
use App\Transformers\BookTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class BookController extends ApiController
{
    /**
     * BookController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('transform.input:'.BookTransformer::class)
            ->only(['store', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $books = Book::all();

        return $this->showAll($books);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'title'       => 'required',
            'description' => 'required',
            'isbn'        => 'required|unique:books|min:10',
            'price'       => 'numeric',
        ];
        $this->validate($request, $rules);
        $fields = $request->all();
        $fields['status'] = Book::STATUS_AVAILABLE;
        $book = Book::create($fields);

        return $this->showOne($book, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  Book  $book
     *
     * @return JsonResponse
     */
    public function show(Book $book)
    {
        return $this->showOne($book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Book  $book
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, Book $book)
    {
        $rules = [
            'isbn'   => 'min:10|unique:books,isbn,'.$book->id,
            'price'  => 'numeric',
            'status' => 'in:'.Book::STATUS_AVAILABLE.','
                .Book::STATUS_UNAVAILABLE,
        ];

        $this->validate($request, $rules);
        if ($request->has('title')) {
            $book->title = $request->title;
        }
        if ($request->has('description')) {
            $book->description = $request->description;
        }
        if ($request->has('isbn')) {
            $book->isbn = $request->isbn;
        }
        if ($request->has('price')) {
            $book->price = $request->price;
        }
        if ($request->has('status')) {
            $book->status = $request->status;
        }
        if ( ! $book->isDirty()) {
            return $this->errorResponse('Update at least one field', 422);
        }
        $book->save();

        return $this->showOne($book);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Book  $book
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return $this->showOne($book);
    }
}
