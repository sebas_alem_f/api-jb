<?php

namespace App\Http\Controllers\Book;

use App\Author;
use App\Book;
use App\Http\Controllers\ApiController;
use App\Transformers\AuthorTransformer;
use Illuminate\Http\Request;

class BookAuthorController extends ApiController
{

    /**
     * BookAuthorController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('transform.input:'.AuthorTransformer::class)
            ->only(['update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Book $book)
    {
        $authors = $book->authors;

        return $this->showAll($authors);
    }

    /**
     * @param  Request  $request
     * @param  Book  $book
     * @param  Author  $author
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Book $book, Author $author)
    {
        $book->authors()->syncWithoutDetaching([$author->id]);

        return $this->showAll($book->authors);

    }

    /**
     * @param  Book  $book
     * @param  Author  $author
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Book $book, Author $author)
    {
        if ( ! $book->authors()->find($author->id)) {
            return $this->errorResponse('Author do not belong to book', 404);
        }

        $book->authors()->detach([$author->id]);

        return $this->showAll($book->authors);

    }

}
