<?php

namespace App\Http\Controllers\Author;

use App\Author;
use App\Http\Controllers\ApiController;
use App\Transformers\AuthorTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthorController extends ApiController
{

    /**
     * AuthorController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('transform.input:'.AuthorTransformer::class)
            ->only(['store', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $authors = Author::all();

        return $this->showAll($authors);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'name'     => 'required',
            'lastname' => 'required',
        ];

        $this->validate($request, $rules);
        $author = Author::create($request->all());

        return $this->showOne($author, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  Author  $author
     *
     * @return JsonResponse
     */
    public function show(Author $author)
    {
        return $this->showOne($author, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Author  $author
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, Author $author)
    {
        if ($request->has('name')) {
            $author->name = $request->name;
        }
        if ($request->has('lastname')) {
            $author->lastname = $request->lastname;
        }

        if ( ! $author->isDirty()) {
            return $this->errorResponse('Update at least one field', 422);
        }
        $author->save();

        return $this->showOne($author, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Author  $author
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Author $author)
    {
        $author->delete();

        return $this->showOne($author);
    }
}
